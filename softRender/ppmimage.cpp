#include "ppmimage.h"
#include <cmath>
#include <iostream>
#include <sstream>
#include <vector>

//=============== Class Color ===============
Color::Color() {
  r = 255;
  g = 255;
  b = 255;
};

Color::Color(uint8_t red, uint8_t green, uint8_t blue) {
  r = red;
  g = green;
  b = blue;
};

void Color::setColor(uint8_t red, uint8_t green, uint8_t blue) {
  r = red;
  g = green;
  b = blue;
};


//=============== Class Image ===============
ppmImage::ppmImage(int32_t w, int32_t h) {
  width = w;
  height = h;
  bufferSize = static_cast<size_t>(w * h);
  sufix = "P6";
  std::stringstream ss;
  ss << w;
  wImg = ss.str();
  std::stringstream ss1;
  ss1 << h;
  hImg = ss1.str();
  maxColorValue = "255";

  pxlArr.resize(bufferSize);
}

ppmImage::ppmImage():ppmImage(800, 600){};

void ppmImage::setColor(Color c){
    std::fill(this->pxlArr.begin(), this->pxlArr.end(), c);
}
