#include "triangle.hpp"
#include "common.h"

//=============== Pixel  ===============
Pixel::Pixel(int x, int y, Color c){
    this->x = x;
    this->y = y;
    color = c;
};

void Pixel::setPixel(int x, int y, Color c){
    this->x = x;
    this->y = y;
    color = c;
};

Pixel::Pixel(){
    x = 0;
    y = 0;
    color.setColor(0,0,0);
};

//=============== Vertex  ===============
Vertex::Vertex(int x, int y, int z){
    this-> x = x;
    this-> y = y;
    this-> z = z;
};

Vertex::Vertex(int x, int y):Vertex(x, y, 0){
};

Vertex::Vertex():Vertex(0, 0, 0){
};

//=============== Line  ===============
Line::Line(Vertex &v0, Vertex &v1,const Color &c){

    if (flipVertical) {
      v0.y = height - v0.y;
      v1.y = height - v1.y;
    }
    if (std::abs(v0.x - v1.x) < std::abs(v0.y - v1.y)) { // if the line is steep, we transpose the image
      std::swap(v0.x, v0.y);
      std::swap(v1.x, v1.y);
      isSwapXY = true;
    }
    if(v0.x < v1.x){
        startVertex = v0;
        finishVertex = v1;
    } else {
        startVertex = v1;
        finishVertex = v0;
    }
//    finishVertex.x--;
    calcPixelPos(startVertex, finishVertex, c);
};

void Line::calcPixelPos(Vertex &start, Vertex &finish, const Color &c){

    linePixels.clear();
    dx = finish.x - start.x;
    dy = std::abs(finish.y - start.y);
    error = dy;
    if(finish.y < start.y){
        direction = -1;
    }

    int y = start.y;
    for (int32_t x = startVertex.x; x < finishVertex.x; x++) {
      error += dy;
      Pixel p;
      if(isSwapXY){
          p.setPixel(y, x, c);
      } else {
           p.setPixel(x, y, c);
      }

      linePixels.push_back(p);
      if (error >= dx) {
        error -= dx;
        y += direction;
      }
    }
    if(isSwapXY){                            //?????????????????????
        std::swap(start.x, start.y);
        std::swap(finish.x, finish.y);
        isSwapXY = true;
    }

    /*for (int x = x0; x <= x1; x++) {
      if (steep) {
        img.pxlArr[static_cast<unsigned long>(x * img.width + y)] = c;
      } else {
        img.pxlArr[static_cast<unsigned long>(y * img.height + x)] = c;
      }

      error2 += derror2;
      if (error2 > dx) {
        y += (y1 > y0 ? 1 : -1);
        error2 -= dx * 2;
      }
    }*/
};

Triangle::Triangle()
{

}
