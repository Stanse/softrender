#ifndef PPMIMAGE_H
#define PPMIMAGE_H

#include <stddef.h>
#include <string>
#include <vector>

//=============== Class Color ===============
class Color {
public:
  Color();
  Color(uint8_t r, uint8_t g, uint8_t b);
  //~Color();
  void setColor(uint8_t r, uint8_t g, uint8_t b);
  // private:
  uint8_t r, g, b;
};

//=============== Class Image ===============
class ppmImage {
public:
  ppmImage();
  ppmImage(int32_t, int32_t);
  int32_t width;
  int32_t height;
  std::string sufix, wImg, hImg, maxColorValue;
  size_t bufferSize;
  std::vector<Color> pxlArr;
  void setColor(Color c);

  bool isFlipVertical = false;
};

#endif // PPMIMAGE_H
