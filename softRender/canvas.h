#ifndef CANVAS_H
#define CANVAS_H

#include "ppmimage.h"
#include <string>

class Canvas {
private:
  int width = 320, height = 240;

public:
  Canvas(int, int);
  Canvas();
  int saveImage(const std::string &path, ppmImage &img);
  int loadImage(const std::string &path, ppmImage &ImageFromFile);
  bool compareImage(const ppmImage &img1, const ppmImage &img2);
  bool compareImage();
};

#endif // CANVAS_H
