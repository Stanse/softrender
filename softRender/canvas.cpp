#include "canvas.h"
#include <fstream>
#include <iostream>
#include <iterator>
#include <stddef.h>

/*Canvas::Canvas(int w, int h) {
  this->width = w;
  this->height = h;
};*/

Canvas::Canvas() { };

int Canvas::saveImage(const std::string &path, ppmImage &img) {
  std::ofstream fout;
  fout.open(path, std::ios::out | std::ofstream::binary);
  if (!fout.is_open()) {
    std::cout << "ERROR! File isn't saved" << std::endl;
    fout.close();
    return 1;
  } else {
    fout << img.sufix << std::endl
         << img.wImg << ' ' << img.hImg << ' ' << img.maxColorValue
         << std::endl;
    fout.write(reinterpret_cast<char *>(&(img.pxlArr.front())),
               static_cast<std::streamsize>(img.pxlArr.size() * sizeof(Color)));
    fout.close();
    std::cout << path << " file saved" << std::endl;
    return 0;
  }
};

int Canvas::loadImage(const std::string &path, ppmImage &ImageFromFile) {
  std::ifstream fin;
  fin.open(path, std::ios::in | std::ofstream::binary);
  if (!fin.is_open()) {
    fin.close();
    throw std::invalid_argument("ERROR! Cant load file!");
  } else {
    std::string fileRow;
    int numOfRow = 0;
    while (!fin.eof()) {
      fileRow.clear();
      std::getline(fin, fileRow);
      switch (numOfRow) {
      case 0: {
        ImageFromFile.sufix = fileRow;
        break;
      }
      case 1: {
        // std::cout << fileRow << std::endl;
        ImageFromFile.wImg = fileRow.substr(0, 3);
        ImageFromFile.hImg =
            fileRow.substr(4, 4); // разобраться с этим моментом
        ImageFromFile.maxColorValue = fileRow.substr(8, 9);
        break;
      }
      case 2: {
        char const *pixels = fileRow.c_str();
        // std::cout << "fileRow = " << fileRow.length() << "\n";
        // std::cout << "ImageFromFile.pxlArr = " << ImageFromFile.pxlArr.size()
        // << "\n";
        for (unsigned long i = 0; i < ImageFromFile.pxlArr.size(); i++) {
          ImageFromFile.pxlArr[i].r = static_cast<uint8_t>(pixels[i * 3]);
          ImageFromFile.pxlArr[i].g = static_cast<uint8_t>(pixels[i * 3 + 1]);
          ImageFromFile.pxlArr[i].b = static_cast<uint8_t>(pixels[i * 3 + 2]);
        }
        break;
      }
      }
      numOfRow++;
    }
    fin.close();
    return 0;
  }
}

bool Canvas::compareImage(const ppmImage &img1, const ppmImage &img2) {
  if (img1.sufix != img2.sufix) {
    return false;
  } else {
    for (unsigned long i = 0; i < img1.pxlArr.size(); i++) {
      if ((img1.pxlArr[i].r != img2.pxlArr[i].r) ||
          (img1.pxlArr[i].g != img2.pxlArr[i].g) ||
          (img1.pxlArr[i].b != img2.pxlArr[i].b)) {
        return false;
      }
    }
    return true;
  }
}
