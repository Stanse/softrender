#include "render.h"
#include <iostream>
#include "common.h"

void drawLine(Line &l, ppmImage &img) {
    for(Pixel p : l.linePixels){
        auto pos = static_cast<unsigned long>(p.y * width + p.x);
        img.pxlArr[pos] = p.color;
    }

};
