#include "ppmimage.h"
#include "canvas.h"
#include "triangle.hpp"
#include "render.h"

const Color black(0, 0, 0);
const Color gray(125, 125, 125);
const Color red(255, 0, 0);
const Color green(0, 255, 0);
const Color yeallow(255, 255, 0);
const Color white(255, 255, 255);

int width = 800;
int height = 600;
bool flipVertical = false;

int main() {
  Canvas canvas;
  ppmImage img(width, height);

  img.setColor(black);
  //flipVertical = true;  // меняем ось У

  Vertex v0(width,0);

  Vertex v1(width-3, 200);

  Line l(v0, v1, white);

  drawLine(l, img);

  canvas.saveImage("myImage.ppm", img);

  return 0;
}
