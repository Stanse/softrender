#ifndef RENDER_H
#define RENDER_H

#include "ppmimage.h"
#include "triangle.hpp"

void drawLine(Line &, ppmImage &);

#endif // RENDER_H
