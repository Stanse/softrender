#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "ppmimage.h"
#include <vector>

class Pixel{
public:
    Pixel(int, int, Color);
    Pixel();
    //~Pixel();
    long int x;
    long int y;
    Color color;
    void setPixel(int, int, Color);
};

class Vertex{
public:
    Vertex(int, int, int);
    Vertex(int, int);
    Vertex();
    int x;
    int y;
    int z;
};

class Line{
public:
    Line(Vertex &, Vertex &, const Color &);
    Line();
    //~Line();
    int dx = 0;
    int dy = 0;
    int error = 0;
    int direction = 1;
    bool isSwapXY = false;
    Vertex startVertex;
    Vertex finishVertex;
    std::vector<Pixel> linePixels;
    void calcPixelPos(Vertex &, Vertex &, const Color &);
};

class Triangle
{
public:
    Triangle();
};

#endif // TRIANGLE_HPP
